package de.grobox.appbundlereporter

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton

private const val EMAIL = "incoming+grote-app-bundle-reporter-21606929-issue-@incoming.gitlab.com"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
    }

    override fun onStart() {
        super.onStart()

        val (hasBundles, text) = SplitGetter.getSplitText(this)
        findViewById<TextView>(R.id.textView).text = text

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        if (hasBundles) {
            fab.show()
            fab.setOnClickListener {
                val subject = text.lines().take(3).joinToString(" ")
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_EMAIL, arrayOf(EMAIL))
                    putExtra(Intent.EXTRA_SUBJECT, subject)
                    putExtra(Intent.EXTRA_TEXT, text)
                    type = "text/plain"
                }
                val shareIntent = Intent.createChooser(sendIntent, null)
                startActivity(shareIntent)
            }
        } else {
            fab.hide()
        }
    }

}
