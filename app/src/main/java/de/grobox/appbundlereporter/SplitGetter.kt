package de.grobox.appbundlereporter

import android.content.Context
import android.os.Build
import android.os.Build.VERSION.SDK_INT
import android.util.DisplayMetrics

object SplitGetter {

    fun getSplitText(context: Context): Pair<Boolean, String> {
        val packageManager = context.packageManager

        val sb = StringBuilder()

        sb.append(Build.SUPPORTED_ABIS.map { it }).appendLine()
        sb.append(getLocale(context)).appendLine()
        sb.append(getScreenDensity(context)).appendLine()
        sb.appendLine()

        var hasBundles = false
        packageManager.getInstalledPackages(0).forEach {
            if (it.splitNames != null) {
                hasBundles = true
                sb.append(it.packageName).appendLine()
                for (i in it.splitNames.indices) {
                    val prefix = "${i + 1}".padStart(4)
                    val revisionCode =
                        if (SDK_INT >= 22) it.splitRevisionCodes[i].toString() else "?"
                    sb.append("$prefix ${it.splitNames[i]} v$revisionCode").appendLine()
//                    sb.append("  ${it.applicationInfo.splitSourceDirs[i]}\n")
                }
                sb.appendLine()
            }
        }
        if (!hasBundles) sb.append("No app bundles installed").appendLine()
        return Pair(hasBundles, sb.toString())
    }

    private fun getScreenDensity(context: Context): String {
        val d = context.resources.displayMetrics.densityDpi
        return when {
            d > DisplayMetrics.DENSITY_XXHIGH -> "xxxhdpi"
            d > DisplayMetrics.DENSITY_XHIGH -> "xxhdpi"
            d > DisplayMetrics.DENSITY_HIGH -> "xhdpi"
            d > DisplayMetrics.DENSITY_MEDIUM -> "hdpi"
            d > DisplayMetrics.DENSITY_LOW -> "mhdpi"
            else -> "lhdpi"
        } + " $d"
    }

    private fun getLocale(context: Context): List<String> {
        return if (SDK_INT >= 24) {
            val locales = context.resources.configuration.locales
            ArrayList<String>(locales.size()).apply {
                for (i in 0 until locales.size()) {
                    add(locales[i].language)
                }
            }
        } else {
            @Suppress("DEPRECATION")
            listOf(context.resources.configuration.locale.language)
        }
    }

}
